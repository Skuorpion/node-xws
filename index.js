(function() {

    //Debut : initialisation
    var msg = {
        type : "",
        id : 0,
        text : ""
    };
    var term = new Terminal;
    term.open(document.getElementById("terminal"));
    let ws;
    //Fin : initialisation
    
    //Debut : Fonction pour evenement Websocket
    function init() {

        //Si il y a des erreurs
        if (ws) {
            ws.onerror = ws.onopen = ws.onclose = null;
            ws.close();
        }

        //Créer un connexion
        ws = new WebSocket('ws://localhost:6969');
                ws.onopen = () => {
            console.log('Connection opened!');
        }

        //Quand on recoit un message
        ws.onmessage=function(event) { 
            var data = JSON.parse(event.data);
            console.log(data);
            switch(data.type)
            {
                //Si c'est pour l'initalisation (reconnaitre la page web)
                case "idInit":
                    msg.id = data.id;
                    break;

                //Ou des données du terminal
                case "term":
                    term.write(data.text);
                    break;
            }
        };

        //Quand la connexion avec le serveur est fermer
        ws.onclose = function() {
            ws = null;
            console.log('Connection closed!');
        }
    }
    //Fin : Fonction pour evenement Websocket


    //Quand on ecrit dans le terminal de la page web (en vrai on n'ecrit pas dedans,
    //ce que tu tape au clavier est direct envoyer au serv
    //et le serv revoi les données que tu a tapé vu qu'il a recu des donnée du terminal)
    term.onData(e => {
        msg.type = "message";
        msg.text = e;
        //console.log(msg.id);
        ws.send(JSON.stringify(msg)); //terminal.toTerm
    });

    init();
})();