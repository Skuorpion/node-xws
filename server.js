const express = require('express');
const http = require('http');
const WebSocket = require('ws');

const port = 6969;
const server = http.createServer(express);
const wss = new WebSocket.Server({ server });

var os = require('os');
var pty = require('node-pty');
var shell = os.platform() === 'win32' ? 'powershell.exe' : 'bash';


var listeClient = [];
class clientInfos {
    constructor(id, infos) {
        this.id = id;
        this.infos = infos;
        this.terminal = pty.spawn(shell, [], {
                            name: 'xterm-color',
                            cols: 80,
                            rows: 24,
                            cwd: process.env.HOME,
                            env: process.env
                        });
    }
}


var clientId = new Set();
var n = 0;
var msg = {
    type : "",
    id : 0,
    text : ""
};

//Début : quand il y a une connexion (le code a l'interieur n'est executé qu'une fois,
//sauf les fonction comme celle qui contiene on qui s'active a chaque fois qu'il se passe ce quelle disent)
wss.on('connection', function connection(ws) {

    //Début : definie un id et créé une instance dans un tableau
    while (clientId.has(n) == true) {
        n++;
    };
    clientId.add(n);
    listeClient[n] = new clientInfos(n, ws);
    //Fin : definie un id et créé une instance dans un tableau

    //Début : envoi un id a la page web
    msg.type = "idInit";
    msg.id = n;
    msg.text = "";
    ws.send(JSON.stringify(msg));
    //Fin : envoi un id a la page web

            
    //Début : quand le terminal recoit/créé des donnée
    listeClient[n].terminal.on('data', function(data, test=n) {
        //console.log(test); //marche pas...
        msg.type = "term";
        msg.id = 0;
        msg.text = data;
        ws.send(JSON.stringify(msg));
    });
    //Fin : quand le terminal recoit/créé des donnée
    n=0;



    //Début : quand la page web envoie des infos
    ws.on('message', function incoming(data) { //terminal.toTerm
        msg = JSON.parse(data);
        listeClient[msg.id].terminal.write(msg.text);
    })
    //Fin : quand la page web envoie des infos

    ws.on('close', function close() {
        listeClient.forEach(function each(client) {
            if (client.infos == ws) {
                clientId.delete(client.id);
            }
        })
    });
})
//Fin : quand il y a une connexion


server.listen(port, function() {
    console.log(`Server is listening on ${port}!`)
})